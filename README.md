# Scripture Databases

A repository of various Bible translation databases. These texts are in the Public Domain.

- **ASV:** American Standard Version
- **BBE:** Bible in Basic English
- **GNT:** Greek New Testament - [Source](https://www.sacred-texts.com/bib/osrc/)
- **JPS:** Jewish Publication Society Tanakh 1917
- **KJV:** King James Version
- **LVB:** Latin Vulgate Bible - Biblia Sacra Juxta Vulgatam Clementinam
    - Psalms aligned with the English translations.
- **LXX-en:** Greek Septuagint - English Translation by Lancelot Charles Lee Brenton
    - This text should be fully aligned with the English translations.
- **LXX-gr:** Greek Septuagint - Greek Translation - [Source](https://www.sacred-texts.com/bib/osrc/)
    - Psalms, Jeremiah, and Jonah are aligned with the English translations. Psalm 151 is included.
- **WBS:** Noah Webster's Common Version
- **WEB:** World English Bible
- **YLT:** Young's Literal Translation

## Database Files

The database files have a `.db` file extension and can be opened in any text editor.

## A Note on the KJV

Residents of the UK, please see the following link: https://en.wikipedia.org/wiki/King_James_Version#Copyright_status
